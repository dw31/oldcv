<!DOCTYPE html>
<html style="height: 100%">
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="icon" type="image/png" href="icon.png" />
		<title>William de-Azevedo</title>
	</head>
	<body link="#FFFFFF" vlink="#FFFFFF" alink="#FFFFFF">
		<div class="top-container">
	   		<img src="photo.png" id="photo" align="right"/>
	   		<div id="top-txt">
	    		<p class="fnt" style="font-size: 20px;">William de-Azevedo</p>
    			<p class="fnt" style="font-size: 16px; font-style: italic;">Developer, Paris</p>
    			<p class="fnt" style="font-size: 30px; font-style: bold; color:red;">PAGE DISCONTINUED - NOT UPDATE</p>

   			</div>
    		</div>
		<div class="mid-container">
			<div id="skill-container">
				<p class="fnt" style="font-size: 10px; margin-right: 4px; text-align: right;">UNIX</p>
				<div class="skill-barL" data-length="20"></div><br/>
				<p class="fnt" style="font-size: 10px; margin-right: 4px; text-align: right;">C/C++</p>
				<div class="skill-barL" data-length="20"></div><br/>
				<p class="fnt" style="font-size: 10px; margin-right: 4px; text-align: right;">WEB (PHP, MYSQL, APACHE...)</p>
				<div class="skill-barL" data-length="50"></div><br/>
				<p class="fnt" style="font-size: 10px; margin-right: 4px; text-align: right;">JAVA</p>
				<div class="skill-barL" data-length="70"></div><br/>
				<p class="fnt" style="font-size: 10px; margin-right: 4px; text-align: right;">PYTHON</p>
				<div class="skill-barL" data-length="70"></div>
			</div>
			<div id="void-container"><p class="fnt title-mid">Skills</p></div>
			<div id="skill-container">
				<p class="fnt" style="font-size: 10px; margin-left: 4px;">FINANCE ENVIRONMENT (SECURE DEV, XML/XSD/XSLT...)</p>
				<div class="skill-barR" data-length="80"></div><br/>
				<p class="fnt" style="font-size: 10px; margin-left: 4px;">BASH/SCRIPTING</p>
				<div class="skill-barR" data-length="70"></div><br/>
				<p class="fnt" style="font-size: 10px; margin-left: 4px;">VERSIONING (GIT/CVS)</p>
				<div class="skill-barR" data-length="60"></div><br/>
				<p class="fnt" style="font-size: 10px; margin-left: 4px;">DOCKER</p>
				<div class="skill-barR" data-length="50"></div><br/>
				<p class="fnt" style="font-size: 10px; margin-left: 4px;">FUNCTIONNAL PROGRAMMATION (OCAML)</p>
				<div class="skill-barR" data-length="20"></div>
			</div>
		</div>
		<div id="aboutme-container">
			<div id="aboutme-txt" class="fnt">About Me</div>
			<p class="fnt" style="font-size: 11px; font-style: italic; margin-left: 85px;">Hello,</p>
			<p class="fnt" style="font-size: 11px; font-style: italic; margin-left: 85px;">My name is William, I have <?php  $date = new DateTime("15-08-1990"); $now = new DateTime(); $interval = $now->diff($date); echo $interval->y;?> years old and I am from Toulouse in France.</p>
			<p class="fnt" style="font-size: 11px; font-style: italic; margin-left: 85px;">I am currently a C++ dev at LusisPayment Paris since 2015.</p>
			<p class="fnt" style="font-size: 11px; font-style: italic; margin-left: 85px;">I graduated from 42 Paris. <br/>It is an IT school for future developers where we view many technologies : C, C++, PHP, Java, iOs/Swift, Ocaml, Docker, MySQL, HTML-CSS, Javascript, LAMP environment, Symfony framework, bash-scripting, python, XML/XSL/XSD, Assembler Intel x86...</p>
		</div>
		<div class="projects-container">
			<p class="fnt title-bot">Examples of school projects</p>
			<img src="project_shootthemup.jpg" class="projects-photo" align="left" style="margin-left: 75px;"/>
			<p class="fnt projects-txtL" style="font-size: 12px; text-decoration: underline; padding-top: 35px;">ft_retro:</p>
			<p class="fnt projects-txtL" style="font-size: 11px; font-style: italic;">Creating a Shoot Them'Up like game.</p>
			<p class="fnt projects-txtL" style="font-size: 11px; font-style: italic;">The game is coded in C++.</p>
			<p class="fnt projects-txtL" style="font-size: 11px; font-style: italic; margin-bottom: 45px;">The graphical part was made with ncurses.</p>
			<img src="project_minishop.jpg" class="projects-photo" align="right" style="margin-right: 75px;"/>
			<p class="fnt projects-txtR" style="font-size: 12px; text-decoration: underline; padding-top: 40px;">ft_minishop:</p>
			<p class="fnt projects-txtR" style="font-size: 11px; font-style: italic;">The purpose of this project was to make a complete shopping site.</p>
			<p class="fnt projects-txtR" style="font-size: 11px; font-style: italic; margin-bottom: 50px;">The technologies were multiple : HTML, CSS, PHP, mySQL, Javascript, Ajax and Nginx/MAMP.</p>
			<img src="project_minishell.jpg" class="projects-photo" align="left" style="margin-left: 75px;"/>
			<p class="fnt projects-txtL" style="font-size: 12px; text-decoration: underline; padding-top: 33px;">ft_minishell:</p>
			<p class="fnt projects-txtL" style="font-size: 11px; font-style: italic;">We created our own shell in C.</p>
			<p class="fnt projects-txtL" style="font-size: 11px; font-style: italic;">It is based on bash (cloned some builtins like cd, env...).</p>
			<p class="fnt projects-txtL" style="font-size: 11px; font-style: italic; margin-bottom: 40px;">I also recoded ls command and printf function.</p>
			<img src="project_wolf3d.jpg" class="projects-photo" align="right" style="margin-right: 75px;"/>
			<p class="fnt projects-txtR" style="font-size: 12px; text-decoration: underline; padding-top: 40px;">wolf3d:</p>
			<p class="fnt projects-txtR" style="font-size: 11px; font-style: italic;">A Wolfenstein 3D fork (<a href="http://en.wikipedia.org/wiki/Wolfenstein_3D" target="_blank">Wikipedia page</a>).</p>
			<p class="fnt projects-txtR" style="font-size: 11px; font-style: italic;">I used raytracing to project the textures.</p>
			<p class="fnt projects-txtR" style="font-size: 11px; font-style: italic; margin-bottom: 50px;">I handled weapons, life, score...</p>
		</div>
		<div id="contact-container">
				<div class="fnt" style="font-size: 14px; margin-left: 100px; margin-top: 50px; width: 200px; height: 100px; font-weight: bold; float: left;";>Contact me : </div>
				<div id="contact-subcontainer"><a href="mailto:william@de-azevedo.fr"><img src="mail.png" class="contact-photo"/></a></div>
				<div id="contact-subcontainer"><img src="mobile.png" class="contact-photo" onclick="alert('My phone number is +33 6 16 07 18 97')"/></div>
				<div id="contact-subcontainer"><a href="https://fr.linkedin.com/in/williamdeazevedo"><img src="linkedin.png" class="contact-photo" href="http://www.google.fr"/></a></div>
				<div id="contact-subcontainer"><a href="https://www.facebook.com/31william31"><img src="fbook.png" class="contact-photo"/></a></div>
		</div>
		<div style="text-align: right; font-size: 11px; color: #E6E6E6; font-style: italic; margin-bottom: 8px;">&copy;<?php echo date("Y"); ?> William de-Azevedo</div>
   	</body>
</html>
